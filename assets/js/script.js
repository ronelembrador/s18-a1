console.log("Hello World");

let trainer = {

	name : "Misty",
	location : "Cerulean City",
	gender : "Female",
	dialogue : function(){
		console.log("My policy is an all-out offensive with Water-type Pokémon!")
	}
}

console.log(trainer.name);
console.log(trainer['location']);
trainer.dialogue();


function Pokemon(name, level, element){
	this.name = name;
	this.level = level;
	this.element = element;
	this.health = 100 + (3*level);
	this.tackle = function(enemy){
		console.log(`${this.name} tackles ${enemy.name}`);
		enemy.health -= 15;
		console.log(`${enemy.name}'s health has dropped to ${enemy.health}`);
	}
}


let pikachu = new Pokemon("Pikachu", 10, "Lightning");
let onyx = new Pokemon("Onyx", 8, "Earth");
let mrMime = new Pokemon("Mr. Mime", 5, "Psychic");

onyx.tackle(mrMime);